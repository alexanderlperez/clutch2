<?php

// Globals
require_once( 'globals.php' );

// Helpers
require_once( 'helpers.php' );

// Enqueue additions
require_once( 'enqueues.php' );

// Category Listings widgets
require_once( 'widgets/category-listing/category-listing.php' );

// Article Listings widgets
require_once( 'widgets/article/article-listing.php' );

// Page "hero" content
require_once( 'widgets/heroes/category.php' );
require_once( 'widgets/heroes/article.php' );

// Posts Shortcodes 
require_once( 'widgets/posts/featured-large.php' );
require_once( 'widgets/posts/narrow.php' );
require_once( 'widgets/posts/half.php' );
require_once( 'widgets/posts/mini-vid.php' );
require_once( 'widgets/posts/featured-large-video.php' );
require_once( 'widgets/posts/full-width-video.php' );
require_once( 'widgets/posts/top-posts.php' );

// Sidebar Shortcodes
require_once( 'widgets/sidebar/trending.php' );
require_once( 'widgets/sidebar/latest.php' );

// Ad Unit Shortcodes
require_once( 'widgets/ad-units/square-ad.php' );
require_once( 'widgets/ad-units/wide-ad.php' );

// Post Customizations
require_once( 'post-customizations.php' );

// Admin Pages
require_once( 'admin/trending-links.php' );

// custom AJAX endpoints
require_once( 'api/top-posts.php' );
require_once( 'api/register-newsletter-email.php' );

// Settings

/*
 *remove unwanted formatting
 */
remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop' , 99);
add_filter( 'the_content', 'shortcode_unautop',100 );

/*
 *remove pages from search results
 */
function search_filter($query) {
    if ( $query->is_search ) {
        $query->set( 'post_type', 'post' );
    }
    return $query;
}
add_filter( 'pre_get_posts', 'search_filter' );

// add the options page
if( function_exists('acf_add_options_page')  ) {
    acf_add_options_page();
}
