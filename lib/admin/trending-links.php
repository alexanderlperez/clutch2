<?php

function trending_links_add_page() {
  //add_options_page( 'Trending Links', 'Trending Links', 'manage_options', 'trending_links', 'trending_links_page' );  
 add_menu_page(
        'Trending Links',
        'Trending Links',
        'manage_options',
        'trending_links',
        'trending_links_page',
        '',
        6 );

  register_setting( 'trending_links_group', 'trending_title_1', 'sanitize_trending_link' );
  register_setting( 'trending_links_group', 'trending_title_2', 'sanitize_trending_link' );
  register_setting( 'trending_links_group', 'trending_title_3', 'sanitize_trending_link' );

  register_setting( 'trending_links_group', 'trending_link_1', 'sanitize_trending_link' );
  register_setting( 'trending_links_group', 'trending_link_2', 'sanitize_trending_link' );
  register_setting( 'trending_links_group', 'trending_link_3', 'sanitize_trending_link' );

  register_setting( 'trending_links_group', 'trending_image_1', 'sanitize_trending_link' );
  register_setting( 'trending_links_group', 'trending_image_2', 'sanitize_trending_link' );
  register_setting( 'trending_links_group', 'trending_image_3', 'sanitize_trending_link' );

  register_setting( 'trending_links_group', 'trending_category_1', 'sanitize_trending_link' );
  register_setting( 'trending_links_group', 'trending_category_2', 'sanitize_trending_link' );
  register_setting( 'trending_links_group', 'trending_category_3', 'sanitize_trending_link' );
}
add_action( 'admin_menu', 'trending_links_add_page' );

function enqueue_admin() {
    wp_enqueue_media();
}
add_action('admin_enqueue_scripts', 'enqueue_admin');

function trending_links_page() {
    include_once( get_template_directory() . '/template-parts/admin/trending-links.php' );
}

function sanitize_trending_link( $input ) {
    // TODO
    return $input;
}
