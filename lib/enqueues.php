<?php

function clutch_enqueue_scripts() {
    wp_enqueue_style( 'jscomposer', get_template_directory_uri() . '/css/js_composer.min.css', false, true );
    wp_enqueue_style( 'featherlight', get_template_directory_uri() . '/css/featherlight.min.css', false, true );

    wp_enqueue_script( 'featherlight', get_template_directory_uri() . '/js/featherlight.min.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'scrolltrigger', get_template_directory_uri() . '/js/ScrollTrigger/scrolltrigger.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'morepostsajax', get_template_directory_uri() . '/js/more-posts-ajax.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), false, true );
}
add_action( 'wp_enqueue_scripts', 'clutch_enqueue_scripts' );


