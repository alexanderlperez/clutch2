<?php

$other_category_link = get_category_link( get_category_by_slug( 'other' )->cat_ID );

// returns category link from post's url, or empty string if no related category
function get_post_category( $post_url ) {
    $id = url_to_postid( $post_url );

    if ( 0 == $id ) {
        return '';
    }

    return get_category_link( get_the_category( $id )[0] );
}


// outputs an image tag using either the attachmed 'featured image', or existing attachment
function clutch_the_post_image( $current_id ) {
     global $wpdb;

     $attachments = $wpdb->get_results("select ID from $wpdb->posts where post_parent = $current_id AND post_type = 'attachment'");

     if ( has_post_thumbnail() ) : 
         the_post_thumbnail( $current_id );
     else :
         ?> <img src="<?php echo wp_get_attachment_url( $attachments[0]->ID ); ?>" alt=""/> <?php
     endif;
}

// outputs an image url using either the attachmed 'featured image', or existing attachment
function clutch_get_the_post_image( $current_id ) {
     global $wpdb;

     $attachments = $wpdb->get_results("select ID from $wpdb->posts where post_parent = $current_id AND post_type = 'attachment'");

     if ( has_post_thumbnail() ) : 
         return wp_get_attachment_url( get_post_thumbnail_id( $current_id ), 'full' );
     else :
         return wp_get_attachment_url( $attachments[0]->ID ); 
     endif;
}
