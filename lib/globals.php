<?php

global $CLUTCH_GLOBALS;

$CLUTCH_GLOBALS = array();

$CLUTCH_GLOBALS[ 'latest_number_posts_default' ] = 4;
$CLUTCH_GLOBALS[ 'latest_number_posts_front_page' ] = 4;
$CLUTCH_GLOBALS[ 'latest_number_posts_category' ] = 4;
$CLUTCH_GLOBALS[ 'latest_number_posts_page' ] = 4;
$CLUTCH_GLOBALS[ 'latest_number_posts_post' ] = 4;
$CLUTCH_GLOBALS[ 'google_adwords_key' ] = '';

$CLUTCH_GLOBALS[ 'facebook' ] = 'https://www.facebook.com/clutchmagformen';
$CLUTCH_GLOBALS[ 'twitter' ] = 'https://twitter.com/Clutch_Mag';
$CLUTCH_GLOBALS[ 'instagram' ] = 'https://www.instagram.com/Clutchmag/';
$CLUTCH_GLOBALS[ 'email' ] = '';
