<?php

add_action( 'wp_ajax_nopriv_register-newsletter-email', 'register_newsletter_email' );
add_action( 'wp_ajax_register-newsletter-email', 'register_newsletter_email' );

function register_newsletter_email() {
    global $CLUTCH_GLOBALS;

    if ( check_ajax_referer( 'register-email', 'nonce' ) ) {
        switch( wp_mail( $CLUTCH_GLOBALS['email'], "New newsletter subscription" . $_POST['email'], $_POST['email'] . "send on " . date("F j, Y, g:i a") ) ) {
            case true: 
                wp_send_json_success();
                break;
            case false:
                wp_send_json_error();
                break;
        }
    }
}
