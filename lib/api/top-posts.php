<?php

add_action( 'wp_ajax_nopriv_top-posts', 'top_posts' );
add_action( 'wp_ajax_top-posts', 'top_posts' );

function top_posts() {
    $amount = $_POST[ 'amount' ];
    $offset = $_POST[ 'offset' ];

    $top_posts = wp_most_popular_get_popular( array( 'limit' => $amount, 'offset' => $offset, 'post_type' => 'post', 'range' => 'all_time' ) );

    if ( count( $top_posts ) ) {

        foreach ( $top_posts as $post ) {
            $post->cover_image = empty( get_attached_media( 'image', $post->ID ) ) ? '' : wp_get_attachment_url( array_values( get_attached_media( 'image', $post->ID ) )[0]->ID );
            $post->category = get_the_category( $post->ID );
            $post->permalink = get_permalink( $post->ID );
        }

        wp_send_json_success( $top_posts );
    } else {
        wp_send_json_error( $top_posts );
    }
}
