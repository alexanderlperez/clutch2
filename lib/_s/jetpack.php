<?php
/**
 * Jetpack Compatibility File.
 *
 * @link https://jetpack.me/
 *
 * @package Clutch_2.0
 */

// Add theme support for Responsive Videos.
add_action( 'after_setup_theme', 'clutch_2_0_jetpack_setup' );
function clutch_2_0_jetpack_setup() {
	add_theme_support( 'jetpack-responsive-videos' );
} 

