<?php 

function wide_ad_shortcode() {
    ob_start();
    include( locate_template('template-parts/ad-units/wide-ad.php') );
    return ob_get_clean();
}
add_shortcode( 'wide_ad', 'wide_ad_shortcode' );

function wide_ad_widget() {
    $vc_params =  array(
        'name' => __( 'Wide Ad Unit', 'clutch_eng' ),
        'base' => 'wide_ad',
        'class' => '',
        'category' => __( 'Content', 'clutch_eng' ),
        'show_settings_on_create' => false,
    );

    vc_map( $vc_params );
}
add_action( 'vc_before_init', 'wide_ad_widget' );


