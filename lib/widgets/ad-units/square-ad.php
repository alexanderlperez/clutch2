<?php 

function square_ad_shortcode() {
    ob_start();
    include( locate_template('template-parts/ad-units/square-ad.php') );
    return ob_get_clean();
}
add_shortcode( 'square_ad', 'square_ad_shortcode' );

function square_ad_widget() {
    $vc_params =  array(
        'name' => __( 'Square Ad Unit', 'clutch_eng' ),
        'base' => 'square_ad',
        'class' => '',
        'category' => __( 'Content', 'clutch_eng' ),
        'show_settings_on_create' => false,
    );

    vc_map( $vc_params );
}
add_action( 'vc_before_init', 'square_ad_widget' );


