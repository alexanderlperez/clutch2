<?php 

 function category_hero_shortcode( $attr ) {
    global $other_category_link;

    $atts = vc_map_get_attributes( 'category_hero', $attr );

    /*
     * Listings - default values
     */

    // handle auto-filling the category field
    // and get the metadata for the post

    $listings = array();

    for ( $i = 0; $i < 4; $i++ ) {
        $post_num = $i + 1;

        $listing = array();

        $post_id = url_to_postid( $atts[ 'post_url_' . $post_num ] );
        $post_category = get_post_category(  $atts[ 'post_url_' . $post_num ] );

        $listing[ 'post_id' ] = $post_id;
        $listing[ 'post_category' ] = $post_category;

        if ( 0 == $post_id || '' ==  $post_category ) {
            $listing[ 'category' ] = __( 'Other', 'clutch_eng' );
            $listing[ 'category_url' ] = $other_category_link;
        } else {
            $listing[ 'category' ] = get_the_category( $post_id )[0]->name;
            $listing[ 'category_url' ] = $post_category;
        }

        // VC doesn't return an array entry for empty fields, so we work with '' as the default here

        if ( '' == $atts[ 'cover_image_' . $post_num ] ) {
            $listing[ 'cover_image' ] = get_template_directory_uri() . '/images/default.jpg';
        } else {
            $listing[ 'cover_image' ] = wp_get_attachment_image_src( $atts[ 'cover_image_' . $post_num ], 'full' )[0];
        }

        $listings[] = $listing;
    }

    ob_start();
    include( locate_template('template-parts/heroes/category.php') );
    return ob_get_clean();
}
add_shortcode( 'category_hero', 'category_hero_shortcode' );

function category_hero_widget() {
    $params = array();

    $params[] = array(
        'param_name' => 'category_slug',
        'heading' => __( 'Category Slug', 'clutch_eng' ),
        'description' => __( 'Paste the Category Slug here (category pages only)', 'clutch_eng' ),
        'type' => 'textfield',
        'holder' => 'div',
        'class' => '',
        'save_always' => true
    );

    for ( $i = 0; $i < 4; $i++ ) {
        $post_num = $i + 1;

        $params[] = array(
            'param_name' => 'post_url_' . $post_num,
            'heading' => __( 'Post URL ' . $post_num, 'clutch_eng' ),
            'description' => __( 'Copy and paste the post URL here', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        );

        $params[] = array(
            'param_name' => 'cover_image_' . $post_num,
            'heading' => __( 'Cover Image ' . $post_num, 'clutch_eng' ),
            'description' => __( 'Select the image to use here', 'clutch_eng' ),
            'type' => 'attach_image',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        );
    }

    $vc_params =  array(
        'name' => __( 'Category Hero', 'clutch_eng' ),
        'base' => 'category_hero',
        'class' => '',
        'category' => __( 'Content', 'clutch_eng' ),
        'show_settings_on_create' => false,
        'params' => $params,
    );

    vc_map( $vc_params );
}
add_action( 'vc_before_init', 'category_hero_widget' );


