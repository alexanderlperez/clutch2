<?php 

 function article_hero_shortcode( $attr ) {
    $atts = vc_map_get_attributes( 'article_hero', $attr );

    /*
     * Listings - default values
     */

    ob_start();
    include( locate_template('template-parts/heroes/article.php') );
    return ob_get_clean();
}
add_shortcode( 'article_hero', 'article_hero_shortcode' );

function article_hero_widget() {

    $vc_params =  array(
        'name' => __( 'Article Hero', 'clutch_eng' ),
        'base' => 'article_hero',
        'class' => '',
        'category' => __( 'Content', 'clutch_eng' ),
        'show_settings_on_create' => false,
    );

    vc_map( $vc_params );
}
add_action( 'vc_before_init', 'article_hero_widget' );
