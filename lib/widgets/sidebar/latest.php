<?php

function latest_shortcode( $attr ) {
    ob_start();
    include( locate_template('template-parts/widgets/latest.php') );
    return ob_get_clean();
}
add_shortcode( 'latest', 'latest_shortcode' );

function latest_widget() {
    $vc_params =  array(
        'name' => __( 'latest Posts', 'clutch_eng' ),
        'base' => 'latest',
        'class' => '',
        'category' => __( 'Content', 'clutch_eng' ),
        'show_settings_on_create' => false,
    );

    vc_map( $vc_params );
}
add_action( 'vc_before_init', 'latest_widget' );
