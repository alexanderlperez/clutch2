<?php 

function trending_shortcode( $attr ) {
    ob_start();
    include( locate_template('template-parts/widgets/trending.php') );
    return ob_get_clean();
}
add_shortcode( 'trending', 'trending_shortcode' );

function trending_widget() {
    $params = array( 
        array(
            'param_name' => 'link_1_category',
            'heading' => __( 'Link 1 Category', 'clutch_eng' ),
            'description' => __( 'Copy and paste the post URL here', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'link_1_title',
            'heading' => __( 'Link 1 Title', 'clutch_eng' ),
            'description' => __( 'Copy and paste the post URL here', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'link_1_url',
            'heading' => __( 'Link 1 URL', 'clutch_eng' ),
            'description' => __( 'Copy and paste the post URL here', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'link_1_image',
            'heading' => __( 'Link 1 Image', 'clutch_eng' ),
            'description' => __( 'Select the image to use here', 'clutch_eng' ),
            'type' => 'attach_image',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'link_2_category',
            'heading' => __( 'Link 2 Category', 'clutch_eng' ),
            'description' => __( 'Copy and paste the post URL here', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'link_2_title',
            'heading' => __( 'Link 2 Title', 'clutch_eng' ),
            'description' => __( 'Copy and paste the post URL here', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'link_2_url',
            'heading' => __( 'Link 2 URL', 'clutch_eng' ),
            'description' => __( 'Copy and paste the post URL here', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'link_2_image',
            'heading' => __( 'Link 2 Image', 'clutch_eng' ),
            'description' => __( 'Select the image to use here', 'clutch_eng' ),
            'type' => 'attach_image',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'link_3_category',
            'heading' => __( 'Link 3 Category', 'clutch_eng' ),
            'description' => __( 'Copy and paste the post URL here', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'link_3_title',
            'heading' => __( 'Link 3 Title', 'clutch_eng' ),
            'description' => __( 'Copy and paste the post URL here', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'link_3_url',
            'heading' => __( 'Link 3 URL', 'clutch_eng' ),
            'description' => __( 'Copy and paste the post URL here', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'link_3_image',
            'heading' => __( 'Link 3 Image', 'clutch_eng' ),
            'description' => __( 'Select the image to use here', 'clutch_eng' ),
            'type' => 'attach_image',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
    );

    $vc_params =  array(
        'name' => __( 'Trending Posts', 'clutch_eng' ),
        'base' => 'trending',
        'class' => '',
        'category' => __( 'Content', 'clutch_eng' ),
        'show_settings_on_create' => false,
        'params' => $params,
    );

    vc_map( $vc_params );
}
add_action( 'vc_before_init', 'trending_widget' );

