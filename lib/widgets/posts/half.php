<?php

function half_shortcode($attr) {
    global $other_category_link;

    $atts = vc_map_get_attributes( 'half', $attr );

    /* 
     * Default rendered values
     */ 

    // handle auto-filling the category field

    if ( 0 == url_to_postid( $atts[ 'post_url' ] ) || '' ==  get_post_category( $atts[ 'post_url' ] ) ) {
        $atts[ 'category' ] = __( 'Other', 'clutch_eng' );
        $atts[ 'category_url' ] = $other_category_link;
    } else {
        $atts[ 'category' ] = get_the_category( url_to_postid( $atts[ 'post_url' ] ) )[0]->name;
        $atts[ 'category_url' ] = get_post_category( $atts[ 'post_url' ] );
    }

    // VC doesn't return an array entry for empty fields, so we work with '' as the default here

    if ( '' == $atts[ 'cover_image' ] ) {
        $atts[ 'cover_image' ] = get_template_directory_uri() . '/images/default.jpg';
    } else {
        $atts[ 'cover_image' ] = wp_get_attachment_image_src( $atts[ 'cover_image' ], 'large' )[0];
    }

    if ( '' == $atts[ 'heading' ] ) {
        $atts[ 'heading' ] = 'Missing Heading';
    }

    ob_start();
    include( locate_template( 'template-parts/post-types/half.php' ) );
    return ob_get_clean();
}
add_shortcode( 'half', 'half_shortcode' );

function half_widget() {
    $params = array(
        array(
            'param_name' => 'post_url',
            'heading' => __( 'Post URL', 'clutch_eng' ),
            'description' => __( 'Copy and paste the post URL here', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'cover_image',
            'heading' => __( 'Post Image', 'clutch_eng' ),
            'description' => __( 'Select the image to use here', 'clutch_eng' ),
            'type' => 'attach_image',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'heading',
            'heading' => __( 'Heading', 'clutch_eng' ),
            'description' => __( 'Keep the heading to X characters', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
    );

    $vc_params =  array(
        'name' => __( 'Half Post', 'clutch_eng' ),
        'base' => 'half',
        'class' => '',
        'category' => __( 'Content', 'clutch_eng' ),
        'show_settings_on_create' => true,
        'params' => $params,
    );

    vc_map( $vc_params );
}
add_action( 'vc_before_init', 'half_widget' );
