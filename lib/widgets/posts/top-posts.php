<?php 

function top_posts_shortcode( $attr ) {
    ob_start();
    include( locate_template('template-parts/post-types/top-posts.php') );
    return ob_get_clean();
}
add_shortcode( 'top_posts', 'top_posts_shortcode' );

function top_posts_widget() {
    $vc_params =  array(
        'name' => __( 'Top Posts', 'clutch_eng' ),
        'base' => 'top_posts',
        'class' => '',
        'category' => __( 'Content', 'clutch_eng' ),
        'show_settings_on_create' => false,
    );

    vc_map( $vc_params );
}
add_action( 'vc_before_init', 'top_posts_widget' );

