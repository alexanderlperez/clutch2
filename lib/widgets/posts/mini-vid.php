<?php

function mini_vid_shortcode($attr) {
    global $other_category_link;

    $atts = vc_map_get_attributes( 'mini_vid', $attr );

    /* 
     * Default rendered values
     */ 

    // handle auto-filling the category field

    if ( 0 == url_to_postid( $atts[ 'post_url' ] ) || '' ==  get_post_category( $atts[ 'post_url' ] ) ) {
        $atts[ 'category' ] = __( 'Other', 'clutch_eng' );
        $atts[ 'category_url' ] = $other_category_link;
    } else {
        $atts[ 'category' ] = get_the_category( url_to_postid( $atts[ 'post_url' ] ) )[0]->name;
        $atts[ 'category_url' ] = get_post_category( $atts[ 'post_url' ] );
    }

    // VC doesn't return an array entry for empty fields, so we work with '' as the default here

    if ( '' == $atts[ 'cover_image' ] ) {
        $atts[ 'cover_image' ] = get_template_directory_uri() . '/images/default.jpg';
    } else {
        $atts[ 'cover_image' ] = wp_get_attachment_image_src( $atts[ 'cover_image' ], 'large' )[0];
    }

    if ( '' == $atts[ 'heading' ] ) {
        $atts[ 'heading' ] = 'Missing Heading';
    }

    ob_start();
    include( locate_template( 'template-parts/post-types/mini-vid.php' ) );
    return ob_get_clean();
}
add_shortcode( 'mini_vid', 'mini_vid_shortcode' );

function mini_vid_widget() {
    $params = array(
        array(
            'param_name' => 'post_url',
            'heading' => __( 'Post URL', 'clutch_eng' ),
            'description' => __( 'Copy and paste the post URL here', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'cover_image',
            'heading' => __( 'Post Image', 'clutch_eng' ),
            'description' => __( 'Select the image to use here', 'clutch_eng' ),
            'type' => 'attach_image',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'video_url',
            'heading' => __( 'Video URL', 'clutch_eng' ),
            'description' => __( 'Post the URL to the video being shown here', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
        array(
            'param_name' => 'heading',
            'heading' => __( 'Heading', 'clutch_eng' ),
            'description' => __( 'Keep the heading to X characters', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
    );

    $vc_params =  array(
        'name' => __( 'Mini Vid Post', 'clutch_eng' ),
        'base' => 'mini_vid',
        'class' => '',
        'category' => __( 'Content', 'clutch_eng' ),
        'show_settings_on_create' => true,
        'params' => $params,
    );

    vc_map( $vc_params );
}
add_action( 'vc_before_init', 'mini_vid_widget' );
