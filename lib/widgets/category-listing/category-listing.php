<?php

// Note: VC modal is rendered first, and associated VC code runs before the shortcode's itself

function category_listing_shortcode( $attr ) {
    global $other_category_link;

    $atts = vc_map_get_attributes( 'category_listing', $attr );

    /* 
     * Default rendered values
     */ 

    ob_start();
    include( locate_template( 'template-parts/category-listing/category-listing.php' ) );
    return ob_get_clean();
}
add_shortcode( 'category_listing', 'category_listing_shortcode' );

function category_listing_widget() {
    $params = array(
        array(
            'param_name' => 'category_slug',
            'heading' => __( 'Category Slug', 'clutch_eng' ),
            'description' => __( 'Copy and paste the category slug here', 'clutch_eng' ),
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'save_always' => true
        ),
    );

    $vc_params =  array(
        'name' => __( 'Category Listing', 'clutch_eng' ),
        'base' => 'category_listing',
        'class' => '',
        'category' => __( 'Content', 'clutch_eng' ),
        'show_settings_on_create' => true,
        'params' => $params,
    );

    vc_map( $vc_params );
}
add_action( 'vc_before_init', 'category_listing_widget' );
