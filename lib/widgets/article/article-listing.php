<?php

// Note: VC modal is rendered first, and associated VC code runs before the shortcode's itself

function article_listing_shortcode( $attr ) {

    $atts = vc_map_get_attributes( 'article_listing', $attr );

    /* 
     * Default rendered values
     */ 

    ob_start();
    include( locate_template( 'template-parts/article/article-listing.php' ) );
    return ob_get_clean();
}
add_shortcode( 'article_listing', 'article_listing_shortcode' );

function article_listing_widget() {
    $vc_params =  array(
        'name' => __( 'Article Listing', 'clutch_eng' ),
        'base' => 'article_listing',
        'class' => '',
        'article' => __( 'Content', 'clutch_eng' ),
        'show_settings_on_create' => true,
    );

    vc_map( $vc_params );
}
add_action( 'vc_before_init', 'article_listing_widget' );

