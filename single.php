<?php
 /**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Clutch_2.0
 */

// TODO: implement page here

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

            <?php 

            // get whether it's featured or not and get the correct post
            require( get_template_directory() . '/template-parts/content-single-featured.php' ); 
            //get_template_part( 'template-parts/content-single' ); 
            ?>

		<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
