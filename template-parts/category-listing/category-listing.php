<div class="category-listing">
     <?php 
         $args = array (
             'offset' => 4,
             'posts_per_page' => 8,
             'orderby' => 'most_recent',
             'no_found_rows' => 'true',
             'category_name' => $atts[ 'category_slug' ],
         );

         $the_query = new WP_Query( $args ); 
     ?>

     <?php if ( $the_query->have_posts() ) : ?>
         <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
             <div class="featured-large post">
                 <a href="<?php echo get_post_permalink(); ?>">
                     <div class="aspect-image fw">
                         <div class="aspect-ratio"></div>

                         <?php clutch_the_post_image( get_the_ID() ); ?>

                         <div class="text-wrapper">
                             <span class="category">
                                 <?php $category = get_the_category(); ?>
                                 <?php echo $category[0]->name; ?>
                             </span>

                             <h3 class="heading">
                                 <span><?php the_title(); ?></span>               
                             </h3>
                         </div>
                     </div>

                     <h4 class="meta"><?php echo ( 'admin' == get_the_author() ? "Clutch Magazine" : get_the_author() ); ?><span class="divider"> | </span><span class="date"><?php the_time( 'n/j/Y' ); ?></span></h4>

                     <p class="caption">
                     <?php echo get_the_excerpt(); ?>
                     </p>
                 </a>
             </div>
         <?php endwhile; ?>

         <?php wp_reset_postdata(); ?>
     <?php else :  ?>
         <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
     <?php endif; ?>
</div>
