<?php
/**
 * Note: this is a kind of hack: the VC-related scaffolding is being used with direct 
 * rendering of the shortcodes, rather than using the actual Visual Composer
 *
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Clutch_2.0
 */

?>

<div class="entry-content">

    <!--begin VC-related scaffolding-->

    <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding vc_general hero-wrapper">
        <div class="reset-padding wpb_column vc_column_container vc_col-sm-12">
            <div class="wpb_wrapper">

                <!--do shortcode-->
                <?php echo do_shortcode( '[article_hero]' ); ?>

            </div>
        </div>
    </div>

    <div class="vc_row-full-width"></div>

    <div class="vc_row wpb_row vc_row-fluid">

        <div class="wpb_column vc_column_container vc_col-sm-8 article-wrapper">
            <div class="wpb_wrapper">

                <!--article template-->
                <?php get_template_part( 'template-parts/article/article-listing' ); ?>

                <?php
                    if ( comments_open() || get_comments_number()  ) :
                        comments_template();
                    endif;
                ?>

            </div>
        </div>

        <div class="wpb_column vc_column_container vc_col-sm-4 sidebar-wrapper">
            <div class="wpb_wrapper">

                <!--put sidebar shortcodes here-->
                <?php echo do_shortcode( '
                [square_ad]
                [trending]
                [latest]
                ' ); ?>

            </div>
        </div>

    </div>

    <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
        <div class="reset-padding wpb_column vc_column_container vc_col-sm-12">
            <div class="wpb_wrapper">

                <?php get_template_part( 'template-parts/post-types/full-width-video' ); ?>

            </div>
        </div>
    </div>

    <!--why are these empty rows here?-->
    <div class="vc_row-full-width"></div>

    <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="wpb_wrapper">

                <!--put top posts shortcode here-->
                <?php echo do_shortcode( '[top_posts]' ); ?>

            </div>
        </div>
    </div>

    <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="wpb_wrapper">

                <!--put wide ad shortcode here-->
                <?php echo do_shortcode( '[wide_ad]' ); ?>

            </div>
        </div>
    </div>

</div><!-- .entry-content -->

