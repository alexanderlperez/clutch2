<script>
function twitterShare(desc, winWidth, winHeight) {
    var winTop = (screen.height / 2) - (winHeight / 2);
    var winLeft = (screen.width / 2) - (winWidth / 2);
    window.open('http://twitter.com/share?text=' + desc, 'twitter', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
}
function fbShare(url, winWidth, winHeight) {
    var winTop = (screen.height / 2) - (winHeight / 2);
    var winLeft = (screen.width / 2) - (winWidth / 2);
    //window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
    title = 'Title';
    descr = 'Test';
    image = '';
    window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
}
</script>


<div class="article-sharing-buttons">
    <?php global $CLUTCH_GLOBALS; ?>
    <div class="facebook"><a href="javascript:fbShare('<?php echo urlencode( "http://clutchmag.co/" . get_the_permalink() ); ?>', 520, 350)"><i class="fa fa-facebook"></i>Share</a></div>
    <div class="twitter"><a href="javascript:twitterShare('<?php echo get_the_title(); ?>', 520, 350)"><i class="fa fa-twitter"></i>Share</a></div>
    <div class="email"><a href="<?php echo $CLUTCH_GLOBALS['email'] || '#'; ?>"><i class="fa fa-envelope"></i>Share</a></div>
</div>
