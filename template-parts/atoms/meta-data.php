<h4 class="meta">
    <?php echo ( 'admin' == get_the_author() ? "<span class='clutch'>Clutch</span> Magazine" : get_the_author() ); ?>
    <span class="divider">| </span>
    <span class="date"><?php the_time( 'n/j/Y' ); ?></span>
</h4>

