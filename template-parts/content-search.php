<?php
/**
* Note: this is a kind of hack: the VC-related scaffolding is being used with direct 
* rendering of the shortcodes, rather than using the actual Visual Composer
*
* Template part for displaying single posts.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package Clutch_2.0
*/

?>

<div class="search-wrapper">
    <div class="entry-content">

        <!--begin VC-related scaffolding-->

        <!--why are these empty rows here?-->
        <div class="vc_row-full-width"></div>

        <div class="vc_row wpb_row vc_row-fluid">

            <div class="wpb_column vc_column_container vc_col-sm-7 article-wrapper">
                <div class="wpb_wrapper">

                    <?php if ( have_posts() ) : ?>

                        <header class="page-header">
                            <h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'clutch-2-0' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
                        </header><!-- .page-header -->

                        <?php while ( have_posts() ) : the_post(); ?>

                            <?php $image_array = array_values( get_attached_media( 'image', get_the_ID() ) );  ?>
                            <?php $image = ! empty( $image_array ) ? wp_get_attachment_url( $image_array[0]->ID ) : null; ?>

                            <div class="featured-large post">
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <div class="aspect-image fw">
                                        <div class="bg-image">
                                            <img src="<?php echo $image; ?>" alt="">
                                            <div class="aspect-ratio"></div>
                                        </div>

                                        <div class="text-wrapper">
                                            <span class="category">
                                                <?php $category = get_the_category(); ?>
                                                <?php echo ! empty( $category ) ? $category[0]->name : 'Culture'; ?>
                                            </span>

                                            <h3 class="heading">
                                                <span><?php echo get_the_title(); ?></span>               
                                            </h3>
                                        </div>
                                    </div>

                                    <h4 class="meta">
                                        <?php echo ( 'admin' == get_the_author() ? "<span class='clutch'>Clutch</span> Magazine" : get_the_author() ); ?>
                                        <span class="divider">| </span>
                                        <span class="date"><?php the_time( 'n/j/Y' ); ?></span>
                                    </h4>

                                    <p class="caption">
                                    <?php echo wp_trim_words( get_the_content(), 20, '...' ); ?>
                                    </p>
                                </a>
                                <hr>
                            </div>

                        <?php endwhile; ?>

                        <?php the_posts_navigation(); ?>

                    <?php else : ?>

                        <h2>No Posts Found</h2>

                    <?php endif; ?>

                </div>
            </div>

            <div class="wpb_column vc_column_container vc_col-sm-5 sidebar-wrapper">
                <div class="wpb_wrapper">

                    <!--put sidebar shortcodes here-->
                    <?php echo do_shortcode( '
                        [square_ad]
                        [trending]
                        [latest]
                    ' ); ?>

                </div>
            </div>

        </div>

        <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
            <div class="reset-padding wpb_column vc_column_container vc_col-sm-12">
                <div class="wpb_wrapper">

                    <!--put full width shortcode here-->
                    <?php echo do_shortcode( '[full_width_video post_url="" cover_image="1444" heading="Testing out the full width video" caption="Testing out the Caption for the full width video. I hope this doesn\'t stretch too far."]' ); ?>

                </div>
            </div>
        </div>

        <!--why are these empty rows here?-->
        <div class="vc_row-full-width"></div>

        <div class="vc_row wpb_row vc_row-fluid">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="wpb_wrapper">

                    <!--put top posts shortcode here-->
                    <?php echo do_shortcode( '[top_posts]' ); ?>

                </div>
            </div>
        </div>

        <div class="vc_row wpb_row vc_row-fluid">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="wpb_wrapper">

                    <!--put wide ad shortcode here-->
                    <?php echo do_shortcode( '[wide_ad]' ); ?>

                </div>
            </div>
        </div>

    </div><!-- .entry-content -->

</div><!-- #post-## -->
