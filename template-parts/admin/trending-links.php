<form action="options.php" method="post">
    <?php settings_fields( 'trending_links_group' ); ?>
    <table>
        <tr>
            <td>
                <label>Trending Title 1 <input type="text" name="trending_title_1" value="<?php echo get_option( 'trending_title_1' ); ?>"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label>Trending category 1 <input type="text" name="trending_category_1" value="<?php echo get_option( 'trending_category_1' ); ?>"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label>Trending Link 1 <input type="text" name="trending_link_1" value="<?php echo get_option( 'trending_link_1' ); ?>"></label>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" class="upload-button" data-num="1" value="Upload/Select Image" />
                <input id="trending_image_1" type="text" name="trending_image_1" value="<?php echo get_option( 'trending_image_1' ); ?>" />
            </td>
        </tr>

        <tr>
            <td>
                <label>Trending Title 2 <input type="text" name="trending_title_2" value="<?php echo get_option( 'trending_title_2' ); ?>"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label>Trending category 2 <input type="text" name="trending_category_2" value="<?php echo get_option( 'trending_category_2' ); ?>"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label>Trending Link 2 <input type="text" name="trending_link_2" value="<?php echo get_option( 'trending_link_2' ); ?>"></label>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" class="upload-button" data-num="2" value="Upload/Select Image" />
                <input id="trending_image_2" type="text" name="trending_image_2" value="<?php echo get_option( 'trending_image_2' ); ?>" />
            </td>
        </tr>

        <tr>
            <td>
                <label>Trending Title 3 <input type="text" name="trending_title_3" value="<?php echo get_option( 'trending_title_3' ); ?>"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label>Trending category 3 <input type="text" name="trending_category_3" value="<?php echo get_option( 'trending_category_3' ); ?>"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label>Trending Link 3 <input type="text" name="trending_link_3" value="<?php echo get_option( 'trending_link_3' ); ?>"></label>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" class="upload-button" data-num="3" value="Upload/Select Image" />
                <input id="trending_image_3" type="text" name="trending_image_3" value="<?php echo get_option( 'trending_image_3' ); ?>" />
            </td>
        </tr>

        <tr>
            <td>
                <input type="submit" value="Save Links">
            </td>
        </tr>
    </table>
</form>

<script>
    jQuery(document).ready(function($) {
        $('.upload-button').click(function(e) {
            e.preventDefault();

            var num = $(this).data('num');

            var custom_uploader = wp.media()
            .on('select', function() {

                var attachment = custom_uploader.state().get('selection').first().toJSON();
                $('#trending_image_' + num).val(attachment.url);

            })
            .open();
        });
    });
</script>
