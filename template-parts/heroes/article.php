<?php 
    $category = get_the_category()[0]->name; 

    $thumb_id = get_post_thumbnail_id();
    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
    $thumb_url = $thumb_url_array[0];

    //debug
    echo '<!--'; 
    var_dump( $thumb_url );
    echo '-->'; 
?>

<div class="article-hero bg-image">
    <?php if ( ! empty( $thumb_url ) ) : ?>
        <img src="<?php echo $thumb_url; ?>">
    <?php endif; ?>

    <div class="content-wrapper">
        <div class="category"><?php echo $category; ?></div>

        <h4 class="heading">
             <span><?php echo get_the_title(); ?></span>
        </h3>

        <?php get_template_part( 'template-parts/atoms/meta-data' ); ?>
    </div>
</div>
