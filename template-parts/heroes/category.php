<?php 
// store the article data in data-* attributes using php
// create the widget fields
?>

<div class="category-hero">
    <div class="content-wrapper">

        <h3 class="heading">
             <span>Clutch Magazine</span>
        </h3>

        <div class="buttons">

        <?php if (is_front_page() ) : foreach ( $listings as $listing ) : ?>

            <a href="<?php echo get_the_permalink( $listing[ 'post_id' ] ); ?>" class="hero-button" 
                data-heading="<?php echo get_the_title( $listing[ 'post_id' ] ); ?>" 
                data-category="<?php echo $listing[ 'category' ]; ?>"
                data-background-image="<?php echo $listing[ 'cover_image' ]; ?>">

                <?php echo get_the_title( $listing[ 'post_id' ] ); ?>
            </a>

        <?php 
                endforeach; 
            else: 

            global $wpdb;

            $args = array (
                'posts_per_page' => '4',
                'orderby' => 'most_recent',
                'no_found_rows' => 'true',
                'category_name' => $atts[ 'category_slug' ],
            );

            $the_query = new WP_Query( $args ); 

            if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 
                $excerpt = get_the_excerpt();
                if ( empty( $excerpt ) ) {
                    $excerpt = wp_trim_words( get_the_content(), 20, '...' );
                }
            ?>

            <a href="<?php echo get_the_permalink(); ?>" class="hero-button" 
                data-heading="<?php echo get_the_title(); ?>" 
                data-category="<?php echo get_the_category()['name']; ?>"
                data-background-image="<?php echo clutch_get_the_post_image( get_the_ID() ); ?>">

                <?php echo get_the_title( $listing[ 'post_id' ] ); ?>
            </a>

            <?php 

            endwhile; endif;
        endif; 

        ?>

        </div>
    </div>
</div>
