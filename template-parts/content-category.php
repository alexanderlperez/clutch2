<?php
/**
 * Note: this is a kind of hack: the VC-related scaffolding is being used with direct 
 * rendering of the shortcodes, rather than using the actual Visual Composer
 *
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Clutch_2.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'featured-article' ); ?>>
    <div class="entry-content">

         <!--begin VC-related scaffolding-->

         <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" data-vc-parallax="1.5" class="vc_row wpb_row vc_row-fluid vc_row-no-padding vc_general vc_parallax vc_parallax-content-moving">
             <div class="reset-padding wpb_column vc_column_container vc_col-sm-12">
                 <div class="wpb_wrapper">

                     <!--do shortcode-->
                     <?php echo do_shortcode( '[article_hero]' ); ?>

                 </div>
             </div>
         </div>

         <!--why are these empty rows here?-->
         <div class="vc_row-full-width"></div>

         <div class="vc_row wpb_row vc_row-fluid">

             <div class="wpb_column vc_column_container vc_col-sm-7 article-wrapper">
                 <div class="wpb_wrapper">

                     <!--article template-->
                     <?php get_template_part( 'template-parts/category-listing/category-listing' ); ?>

                 </div>
             </div>

             <div class="wpb_column vc_column_container vc_col-sm-5 sidebar-wrapper">
                 <div class="wpb_wrapper">

                     <!--put sidebar shortcodes here-->
                     <?php echo do_shortcode( '
                         [square_ad]
                         [trending]
                         [latest]
                     ' ); ?>

                 </div>
             </div>

         </div>

         <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
             <div class="reset-padding wpb_column vc_column_container vc_col-sm-12">
                 <div class="wpb_wrapper">

                     <!--put full width shortcode here-->
                     <?php echo do_shortcode( '[full_width_video post_url="" cover_image="1444" heading="Testing out the full width video" caption="Testing out the Caption for the full width video. I hope this doesn\'t stretch too far."]' ); ?>

                 </div>
             </div>
         </div>

         <!--why are these empty rows here?-->
         <div class="vc_row-full-width"></div>

         <div class="vc_row wpb_row vc_row-fluid">
             <div class="wpb_column vc_column_container vc_col-sm-12">
                 <div class="wpb_wrapper">

                     <!--put top posts shortcode here-->
                     <?php echo do_shortcode( '[top_posts]' ); ?>

                 </div>
             </div>
         </div>

         <div class="vc_row wpb_row vc_row-fluid">
             <div class="wpb_column vc_column_container vc_col-sm-12">
                 <div class="wpb_wrapper">

                     <!--put wide ad shortcode here-->
                     <?php echo do_shortcode( '[wide_ad]' ); ?>

                 </div>
             </div>
         </div>

    </div><!-- .entry-content -->

</article><!-- #post-## -->
