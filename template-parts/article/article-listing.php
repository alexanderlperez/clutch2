<div id="post-<?php the_ID(); ?>" class="article-content-wrapper" <?php post_class( 'featured-article' ); ?>>
    <?php the_content(); ?>

    <?php get_template_part( 'template-parts/atoms/article', 'sharing' ); ?>
</div>
