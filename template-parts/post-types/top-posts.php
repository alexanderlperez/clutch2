<?php
    global $wpdb;
    global $post;
    $posts = wp_most_popular_get_popular( array( 'limit' => 2, 'post_type' => 'post', 'range' => 'all_time' ) );
?>

<div class="top-posts-wrapper">
    <h2 class="top-posts-heading">Top Posts</h2>

    <div class="post-wrapper">
        <?php if ( count( $posts ) > 0 ): foreach ( $posts as $post ): setup_postdata( $post ); ?>

            <div class="top-post post">
                <a href="<?php echo get_post_permalink(); ?>">
                    <div class="image-wrapper">
                        <div class="bg-image aspect-image">
                            <img src="<?php echo wp_get_attachment_url( array_values( get_attached_media( 'image' ) )[0]->ID ); ?>" alt="">
                            <div class="aspect-ratio"></div>
                        </div>
                    </div>

                    <span class="category"><?php echo get_the_category()[0]->name; ?></span>

                    <?php include( get_template_directory() . '/template-parts/atoms/meta-data.php' ); ?>

                    <div class="text-wrapper">
                        <h3 class="heading"><?php the_title(); ?></h3>
                        <p class="caption"><?php echo get_the_excerpt(); ?></p>
                    </div>
                </a>
            </div>

        <?php endforeach; endif; ?>
        <?php wp_reset_postdata(); ?>
    </div>

</div>

<?php /*
<div class="js-load-more more-trigger"><a href="#">More Top Articles</a></div>
<div class="top-posts-results"></div>

<div class="top-post-template" style="display:none;">
    <div class="top-post post">
        <a class="permalink" href="">
            <div class="image-wrapper">
                <div class="bg-image aspect-image">
                    <img src="" alt="">
                    <div class="aspect-ratio"></div>
                </div>
            </div>

            <span class="category"></span>

            <h4 class="meta">
                <span class="author"></span> 
                <span class="divider">| </span>
                <span class="date"></span>
            </h4>

            <div class="text-wrapper">
                <h3 class="heading"></h3>
                <p class="caption"></p>
            </div>
        </a>
    </div>
</div>
*/ ?>
