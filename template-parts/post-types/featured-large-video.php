<?php $rand = rand(); ?>
<div class="featured-large-video-post">
    <div id="video-<?php echo $rand; ?>" style="display:none;"><iframe width="560" height="315" src="<?php echo $atts[ 'video_url' ]; ?>" frameborder="0" allowfullscreen></iframe></div>

    <a href="javascript:document.getElementById('video-<?php echo $rand; ?>').style.display = 'block'; document.getElementById('videopic-<?php echo $rand; ?>').style.display = 'none'; void(0);">
        <div id="videopic-<?php echo $rand; ?>" class="wrapper">
            <div class="aspect-image fw">
                <div class="bg-image">
                    <img src="<?php echo $atts[ 'cover_image' ]; ?>" alt="">
            
                    <div class="aspect-ratio"></div>
                </div>
            </div>
            
            <div class="text-wrapper">
                <span class="category">
                    <?php echo $atts[ 'category' ]; ?>
                </span>
            
                <h3 class="heading">
                    <span><?php echo $atts[ 'heading' ]; ?></span>               
                </h3>
            </div>
            
            <?php include( get_template_directory() . '/template-parts/atoms/meta-data.php' ); ?>
        </div>
            
        <p class="caption">
            <?php echo $atts[ 'caption' ]; ?>
        </p>
    </a>
</div>
