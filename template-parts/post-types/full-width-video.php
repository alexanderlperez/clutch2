<?php 

// TODO: add markup to support video iframes

?>

<div class="full-width post">
    <a href="<?php echo isset( $atts[ 'post_url' ] ) ?  $atts[ 'post_url' ] : get_field( 'video_url', 'option' ); ?>" data-featherlight="iframe" data-iframeHeight="50%">

        <div class="aspect-image fw">
            <div class="bg-image">
                <img src="<?php echo isset( $atts[ 'cover_image' ] ) ?  $atts[ 'cover_image' ] : get_field( 'video_cover_image', 'option' ); ?>" alt="">
                <div class="aspect-ratio"></div>
            </div>
        </div>

        <div class="player-icon">
            <img src="<?php echo get_template_directory_uri() . '/images/player-icon.png'; ?>" alt="">
        </div>                                                                                     

        <div class="text-wrapper">                                                                                 
            <h2 class="heading">
                <span><?php echo isset( $atts[ 'heading' ] ) ?  $atts[ 'heading' ] : get_field( 'video_heading', 'option' ); ?></span>
            </h2>                                                            

            <h3 class="caption">
                <span><?php echo isset( $atts[ 'caption' ] ) ?  $atts[ 'caption' ] : get_field( 'video_caption', 'option' ); ?></span>
            </h3>                                 
        </div>
    </a>
</div>

