<?php $rand = rand(); ?>
<div class="mini-vid post">
    <div id="video-<?php echo $rand; ?>" style="display:none;"><iframe width="560" height="315" src="<?php echo $atts[ 'video_url' ]; ?>" frameborder="0" allowfullscreen></iframe></div>

    <a href="javascript:document.getElementById('video-<?php echo $rand; ?>').style.display = 'block'; document.getElementById('videopic-<?php echo $rand; ?>').style.display = 'none'; void(0);">
        <div id="videopic-<?php echo $rand; ?>" class="aspect-image fw">
            <div class="bg-image">
                <img src="<?php echo $atts[ 'cover_image' ]; ?>" alt="">
                <div class="aspect-ratio"></div>
            </div>

            <div class="text-wrapper">
                <span class="category"><?php echo $atts[ 'category' ]; ?></span>

                <h3 class="heading">
                    <span><?php echo $atts[ 'heading' ]; ?></span>
                </h3>
            </div>
        </div>
    </a>
</div>
