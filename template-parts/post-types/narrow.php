<div class="narrow post clearfix">
    <a href="<?php echo $atts[ 'post_url' ]; ?>">
        <div class="aspect-image">
            <div class="bg-image">
                <img src="<?php echo $atts[ 'cover_image' ]; ?>" alt="">
                <div class="aspect-ratio"></div>
            </div>
        </div>

        <div class="text-wrapper">
            <span class="category">
                <?php echo $atts[ 'category' ]; ?>
            </span>

            <h3 class="heading">
                <?php echo $atts[ 'heading' ]; ?>
            </h3>

            <?php include( get_template_directory() . '/template-parts/atoms/meta-data.php' ); ?>

            <p class="caption">
                <?php echo $atts[ 'caption' ]; ?>
            </p>
        </div>
    </a>
</div>

<hr class="narrow">
