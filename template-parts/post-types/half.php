<div class="half post">
    <a href="<?php echo $atts[ 'post_url' ]; ?>">
        <div class="aspect-image fw">
            <div class="bg-image">
                <img src="<?php echo $atts[ 'cover_image' ]; ?>" alt="">
                <div class="aspect-ratio"></div>
            </div>

            <div class="text-wrapper">
                <span class="category"><?php echo $atts[ 'category' ]; ?></span>

                <h3 class="heading">
                    <span><?php echo $atts[ 'heading' ]; ?></span>
                </h3>
            </div>
        </div>
    </a>
</div>
