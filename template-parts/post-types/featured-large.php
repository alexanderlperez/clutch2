<div class="featured-large post">
    <a href="<?php echo $atts[ 'post_url' ]; ?>">

        <div class="aspect-image fw">

            <div class="bg-image">
                <img src="<?php echo $atts[ 'cover_image' ]; ?>" alt="">
                <div class="aspect-ratio"></div>
            </div>

        </div>

        <div class="text-wrapper">
            <span class="category">
                <?php echo $atts[ 'category' ]; ?>
            </span>

            <h3 class="heading">
                <span><?php echo $atts[ 'heading' ]; ?></span>               
            </h3>
        </div>

        <?php include( get_template_directory() . '/template-parts/atoms/meta-data.php' ); ?>

        <p class="caption">
            <?php echo $atts[ 'caption' ]; ?>
        </p>
    </a>
   <hr>
</div>
