<div class="trending-widget">
    <h2>Trending Right Now</h2>

    <?php 
    $max = 3;
    for ($i = 1; $i <= $max; $i++) {
        $title = get_option( 'trending_title_' . $i );
        $permalink = get_option( 'trending_link_' . $i );
        $image = get_option( 'trending_image_' . $i );
        $category = get_option( 'trending_category_' . $i );
    ?>

     <div class="trending post clearfix">
         <a href="<?php echo $permalink; ?>">
             <div class="hover-wrapper">
                 <div class="img-wrapper">

                     <?php if ( empty( $image ) ) : ?>
                         <img src="http://placehold.it/100x100" alt="">
                     <?php else : ?>
                         <img src="<?php echo $image; ?>" alt="">
                     <?php endif; ?>

                 </div>

                 <div class="text-wrapper">

                     <?php if ( empty( $category ) ) : ?>
                         <span class="sidebar-category">No Category</span>
                     <?php else : ?>
                         <span class="sidebar-category"><?php echo $category; ?></span>
                     <?php endif; ?>

                     <?php if ( empty( $title ) ) : ?>
                         <div class="heading">No Title</div>
                     <?php else : ?>
                         <div class="heading"><?php echo $title; ?></div>
                     <?php endif; ?>

                 </div>
             </div>
         </a>
     </div>

     <?php } ?>
</div>


