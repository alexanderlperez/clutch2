<?php 

// This will be modified by having a link set to it

?>
<div class="latest-widget">
    <h2>Latest Articles From <span class="highlight">Clutch</span></h2>

        <?php 
        global $CLUTCH_GLOBALS;

        $max = $CLUTCH_GLOBALS[ 'latest_number_posts_default' ];

        if ( is_front_page() ) {
            $max = $CLUTCH_GLOBALS[ 'latest_number_posts_front_page' ];
        } else if ( is_single() ) {
            $max = $CLUTCH_GLOBALS[ 'latest_number_posts_post' ];
        } else if ( is_page() ) {
            $max = $CLUTCH_GLOBALS[ 'latest_number_posts_page' ];
        } 

        global $wpdb;
        $args = array (
            'posts_per_page' => $max,
            'orderby' => 'most_recent',
            'no_found_rows' => 'true',
        );

        $the_query = new WP_Query( $args ); 

        if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 
        ?>

        <div class="latest post clearfix">
            <a href="<?php echo get_post_permalink(); ?>">

                <div class="img-wrapper">
                    <div class="bg-image aspect-image">
                        <?php clutch_the_post_image( get_the_ID() ); ?>
                        <div class="aspect-ratio"></div>
                    </div>
                </div>
                
                <div class="text-wrapper">
                    <div class="latest-meta"><span class="category"><?php echo get_the_category()[0]->name; ?></span><span class="divider"> | </span><span class="date"><?php the_time( 'n/j/Y' ); ?></span></div>
                    <h3 class="heading"><?php the_title(); ?></h3>
                    <p class="caption"><?php echo get_the_excerpt(); ?></p>
                </div>
            </a>
        </div>

         <?php 
         endwhile;
         wp_reset_postdata();
         else : 
         ?>

        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

        <?php endif; ?>
        <!-- end of the loop -->
    
</div>
