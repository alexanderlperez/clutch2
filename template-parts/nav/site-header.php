<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'clutch-2-0' ); ?></a>

<header id="masthead" class="site-header" role="banner">
    <div class="top">
        <i class="menu-toggle fa fa-navicon"></i>
        <i class="search-toggle fa fa-search"></i>

        <div class="search-bar">
            <form id="search-form" role="search" method="get" action="<?php echo get_site_url(); ?>">
                <input class="search-input" type="search" id="s" value="" name="s" type="text" placeholder="Search CLUTCH">
            </form>
        </div>

        <div class="social">
            <a href="https://www.facebook.com/clutchmagformen"><i class="fa fa-facebook"></i></a>
            <a href="http://twitter.com/Clutch_Mag"><i class="fa fa-twitter"></i></a>
            <a href="http://instagram.com/Clutchmag"><i class="fa fa-instagram"></i></a>
        </div>

        <div class="logo">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <img src="<?php echo get_template_directory_uri() . '/images/clutchlogo.png'; ?>" alt="">
            </a>
        </div>
    </div>

    <div class="menu">
        <i class="menu-toggle fa fa-navicon"></i>
        <i class="search-toggle fa fa-search"></i>

        <div class="search-bar">
            <form id="search-form" role="search" method="get" action="<?php echo get_site_url(); ?>">
                <input class="search-input" type="search" id="s" value="" name="s" type="text" placeholder="Search CLUTCH">
            </form>
        </div>

        <div class="logo">
            <img src="<?php echo get_template_directory_uri() . '/images/clutchlogo.png'; ?>" alt="">
        </div>

        <div class="items">
            <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
        </div>
    </div>
</header><!-- #masthead -->
