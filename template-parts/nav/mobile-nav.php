<nav class="mobile-nav">

    <div class="logo">
        <img src="<?php echo get_template_directory_uri() . '/images/clutchlogo.png'; ?>" alt="">
    </div>

    <ul class="menu">
        <?php wp_nav_menu( array( 'theme_location' => 'primary'  )  ); ?>
    </ul>

    <div class="social">
        <a href=""><i class="fa fa-facebook"></i></a>
        <a href=""><i class="fa fa-twitter"></i></a>
        <a href=""><i class="fa fa-instagram"></i></a>
    </div>

</nav>
