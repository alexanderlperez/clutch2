<?php 
/* Template Name: Clutch Front Page/Category Page
 *
 * @link https://developer.wordpress.org/themes/template-files-section/page-template-files/page-templates/#Custom_Page_Template
 *
 * @package Clutch_2.0
 */

get_header(); ?>

    <div id="primary" class="content-area <?php echo is_front_page() ? 'home-page' : 'category-page'; ?>">
        <main id="main" class="site-main" role="main">
            <div class="top-layout">

                <?php while ( have_posts() ) : the_post(); ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <div class="entry-content">
                            <?php the_content(); ?>
                        </div>
                    </article><!-- #post-## -->

                <?php endwhile; // End of the loop. ?>

            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_footer(); ?>
