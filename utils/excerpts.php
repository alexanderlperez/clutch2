<?php

$query = new WP_Query( array(
    'posts_per_page' => -1,
) );

foreach ( $query->posts as $post ) {
    setup_postdata( $post );

    $sentence = preg_replace( '/(.*?[?!.](?=\s|$)).*/', '\\1', strip_tags( get_the_content() ) );

    update_field( 'field_56f6b0294e9b3', html_entity_decode( $sentence ), $post->ID );
}
