<?php

global $wpdb;

$query = new WP_Query( array(
    'posts_per_page' => -1,
    'post_type' => 'post',
) );

foreach ( $query->posts as $post ) {
    setup_postdata( $post );

    $thumb_id = get_post_thumbnail_id();
    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
    $thumb_url = $thumb_url_array[0];

    $attachments = $wpdb->get_col("select ID from $wpdb->posts where post_parent = $post->ID AND post_type = 'attachment'");
    $image_id = ! empty( $attachments ) ? $attachments[0] : false;

    echo "post... $post->ID \n";
    var_dump( $attachments );

    if ( empty( $thumb_id ) && false !== $image_id) {
        echo "empty post thumbnail $thumb_url, replacing with image id $image_id\n";
        set_post_thumbnail( $post->ID, $image_id );
    } else {
        echo "post $post->ID has no attached media to port\n";
    }
}
