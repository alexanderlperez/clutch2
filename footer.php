<?php
/**
* The template for displaying the footer.
*
* Contains the closing of the #content div and all content after.
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package Clutch_2.0
*/

?>

</div><!-- #content -->

<footer class="site-footer">
    <div class="menu clearfix">
        <?php wp_nav_menu( array( 'theme_location' => 'primary'  )  ); ?>
    </div>

    <div class="social">
        <?php global $CLUTCH_GLOBALS; ?>
        <?php $fb_link = $CLUTCH_GLOBALS['facebook']; ?>
        <?php $t_link = $CLUTCH_GLOBALS['twitter'];  ?>
        <?php $i_link = $CLUTCH_GLOBALS['instagram'];  ?>

        <a href="<?php echo ! empty( $fb_link ) ? $fb_link : '#'; ?>"><span class="fa fa-facebook"></span></a>
        <a href="<?php echo ! empty ( $t_link ) ? $t_link :  '#'; ?>"><span class="fa fa-twitter"></span></a>
        <a href="<?php echo ! empty( $i_link ) ? $i_link : '#'; ?>"><span class="fa fa-instagram"></span></a>
    </div>

    <div class="copyright">
        &copy; 2015 Clutch Magazine.  All Rights Reserved.
    </div>
</footer>

</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
