var $ = jQuery;

$(document).ready(function () {

    ////////////////////
    //  General Init  //
    ////////////////////

    
    create_bg_images();

    //////////////////
    //  search bar  //
    //////////////////

    
    $('.top .search-toggle').on('click', function (e) { 
        e.stopPropagation();
        $('.items').toggleClass('invisible');
        $('.menu .logo').toggleClass('invisible');
        $('.top .search-bar').toggleClass('active').find('input').focus();
    });

    $('.menu .search-toggle').on('click', function (e) { 
        e.stopPropagation();
        $('.items').toggleClass('invisible');
        $('.menu .logo').toggleClass('invisible');
        $('.menu .search-bar').toggleClass('active').find('input').focus();
    });

    //////////////////
    //  Mobile Nav  //
    //////////////////


    $('.menu-toggle').click(function (e) {
        e.stopPropagation();
        $('.mobile-nav').toggleClass('open');
        $('#page').toggleClass('shift-right');
        $('#shadow').toggleClass('active');

        if ($('.mobile-nav').hasClass('open')){
            disableScroll();
        } else {
            enableScroll();
        }
    });

    $('#page, #shadow').click(function () {
        $('.mobile-nav').removeClass('open');
        $('#page').removeClass('shift-right');
        $('#shadow').removeClass('active');
        enableScroll();
    });

    /////////////////////
    //  Sticky Header  //
    /////////////////////
    

    // Set up the header sticky transitions
    var setUpSticky = function() {
        var $mark;

        if ($('.category-hero').length > 0) { 
            $mark = $('.category-hero');
        } else if ($('.article-hero').length > 0) {
            $mark = $('.article-hero');
        } else if ($('.site-header').length > 0) {
            $mark = $('.site-header');
        } else {
            return;
        }

        // make sure we're operating on a rendered object
        if (typeof $mark.height() == "number") {
            var pos = $mark.offset().top + $mark.height();

            ScrollTrigger({
                triggerPosition: pos,
                triggerDownCallback: function() {
                    // only trigger JS stickyness if the screen is large enough for the full-size nav
                    // otherwise CSS takes care of making the nav sticky for tablet/mobile

                    if (document.documentElement.clientWidth >= 1024){
                        $('.top').hide();
                        $('.site-header').addClass('sticky');
                        $('.top .logo').hide();
                        $('.site-header .menu .logo').show();
                    }
                },
                triggerUpCallback: function() {
                    $('.top').show();
                    $('.site-header').removeClass('sticky');
                    $('.top .logo').show();
                    $('.site-header .menu .logo').hide();
                }
            });
        } else {
            setTimeout(setUpSticky, 100);
        }
    };
    setTimeout(setUpSticky, 100)

    ////////////////////////////
    //  Email Subscribe Form  //
    ////////////////////////////
    
    $('#submit-email').click(function (evt) {
        evt.preventDefault();

        if ( $('#registration-email').val() !== "" ) {
            var ajaxUrl = '/clutch2/wp-admin/admin-ajax.php';
            var data = {
                action: 'register-newsletter-email',
                email: $('#registration-email').serialize(),
                nonce: $('#_wpnonce').val(),
                referrer: $('[name="_wp_http_referer"]').val(),
            };

            // submit to email.php
            $.post(ajaxUrl, data).then(function (data) {
                if ( data.success === true ){
                    $('#registration-form').fadeOut(500, function () {
                        $('#registration-sent').show();
                    });
                }
            });
        }
    });

});

$(window).load(function () {

    /////////////////////
    //  Category Hero  //
    /////////////////////
    

    var $buttons = $('.category-hero .hero-button');

    // preload hero bg images
    $buttons.each(function (i, e) {
        var url = $(e).data('background-image');
        var img = new Image();

        $(img).addClass('bg-image-' + i).hide()
            .prependTo('.category-hero');

        // show the first image + heading
        if (i == 0) {
            img.onload = (function () {
                return function () {
                    $('.category-hero .heading span').text($($buttons[0]).data('heading'));
                    $('.category-hero').css({ 
                        'background-image': 'url(' + url + ')',
                        'background-repeat': 'no-repeat',
                        'background-size': 'cover',
                    });
                }
            })();
        }

        img.src = url;
    });

    // handle the hero button hover effects
    $buttons.each(function (i, e) {
        $(e).hover(function () {
            var url = $(this).data('background-image');

            // change the bg-image
            $('.category-hero').css({ 
                'background-image': 'url(' + url + ')',
                'background-repeat': 'no-repeat',
                'background-size': 'cover',
            });

            // change the heading
            var heading = $(e).data('heading');
            var category = $(e).data('category');
            $('.category-hero .heading span').text(heading);
            $('.category-hero .category').text(category);
            $('.category-hero .caption').html($($buttons[0]).data('caption'));
        });
    });

    /////////////////////
    //  Resize Events  //
    /////////////////////

    function stretchContent() {

        $('[data-vc-stretch-content="true"]').each(function (i, e) {
            var fullWidth = $(window).width();

            $(e).width(fullWidth);

            $(window).resize(resizeIt);
            resizeIt(e);

        });
    }

    function resizeIt(e) {
        var fullWidth = $(window).width();
        if (window.matchMedia("(min-width: 767px)").matches) {
            $(e).css({
                'position': 'relative',
                'left': '-6px',
                'box-sizing': 'border-box',
                'width': fullWidth, // TODO: this should be dynamic
            });
        } else  {
            console.log(fullWidth);
            $(e).css({
                'left': '0',
                'margin': '0 !important',
                'width': 'initial', // TODO: this should be dynamic
            });
        }
    }

    stretchContent();
});


////////////////////////
//  helper functions  //
////////////////////////


function create_bg_images() {
    $( ".bg-image" ).each(function() {
        var bg_image = $(this).children("img");
        var img_url = bg_image.attr("src");

        $(this).css({
            "background-image": "url(" + img_url + ")",
            "background-size": "cover",
        });

        bg_image.remove();
    });
}

// from: http://stackoverflow.com/a/4770179/1817379
// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
  e = e || window.event;
  if (e.preventDefault)
      e.preventDefault();
  e.returnValue = false;  
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
  if (window.addEventListener) // older FF
      window.addEventListener('DOMMouseScroll', preventDefault, false);
  window.onwheel = preventDefault; // modern standard
  window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
  window.ontouchmove  = preventDefault; // mobile
  document.onkeydown  = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null; 
    window.onwheel = null; 
    window.ontouchmove = null;  
    document.onkeydown = null;  
}
