/**
 * Expects:
 * - .js-load-more
 * - .top-post-template
 * - .results
 */

;(function (window, $) {
    $('.js-load-more').click(function (e) {
        e.preventDefault();
        console.log($('.top-post.post').length);
        queryData({ offset: $('.top-post.post').length });
    });

    function queryData( data ) {
        $postsWrapper = $('.top-posts-results');
        $responseTemplate = $('.top-post-template');

        var ajaxUrl = '/clutch2/wp-admin/admin-ajax.php';
        var params = {
            action: 'top-posts',
            amount: '5',
            offset: data.offset,
        };

        $.post( ajaxUrl, params, function renderEventsResponse( response ) {

            // render template items from data

            console.log(response);

            response[ 'data' ].forEach(function (item, i) {
                var entry = $responseTemplate.clone();

                var title = item.post_title.length > 0 ? item.post_title : "You just won't believe it until you read it!";
                var excerpt = item.post_excerpt.length > 0 ? item.post_excerpt : stripTags(cutString(item.post_content, 200)) + '...';
                var category = item.category[0] ? item.category[0].cat_name : 'Culture';
                var permalink = item.permalink.length > 0 ? item.permalink : '#';
                var coverImage = item.cover_image ? item.cover_image : '';

                entry.find('.heading').text(title);
                entry.find('.caption').text(excerpt);
                entry.find('.category').text(category);
                entry.find('.permalink').attr('href', permalink);

                entry.removeClass('top-post-template');
                entry.appendTo($postsWrapper);
                entry.show();
            });

        }, 'json')
        .error( function ( jqXHR, textStatus, error ) {
            console.log( "Error: could not get data", arguments );
        } );
    }

    // from: https://css-tricks.com/snippets/javascript/strip-html-tags-in-javascript/
    function stripTags(string) {
        return string.replace(/(<([^>]+)>)/ig,"");
    }

    // from: http://stackoverflow.com/a/5454380/1817379
    function cutString(s, n){
        var cut= s.indexOf(' ', n);
        if(cut== -1) return s;
        return s.substring(0, cut)
    }
})(window, jQuery);
