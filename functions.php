<?php
add_action( 'after_setup_theme', 'clutch_2_0_setup' );
if ( ! function_exists( 'clutch_2_0_setup' ) ) :
function clutch_2_0_setup() {
	load_theme_textdomain( 'clutch-2-0', get_template_directory() . '/languages' );

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	add_theme_support( 'custom-background', apply_filters( 'clutch_2_0_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'clutch-2-0' ),
	) );
}
endif;

add_action( 'widgets_init', 'clutch_2_0_widgets_init' );
function clutch_2_0_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'clutch-2-0' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}

add_action( 'wp_enqueue_scripts', 'clutch_2_0_scripts' );
function clutch_2_0_scripts() {
	wp_enqueue_style( 'clutch-2-0-style', get_stylesheet_uri() );

	wp_enqueue_script( 'clutch-2-0-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', array(''), '', true  );
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '', true  );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

// _s functions
require get_template_directory() . '/lib/_s/template-tags.php'; // Custom template tags for this theme.
require get_template_directory() . '/lib/_s/customizer.php'; // Customizer additions.
require get_template_directory() . '/lib/_s/jetpack.php'; // Load Jetpack compatibility file.

// Clutch 2 custom functions
require get_template_directory() . '/lib/clutch.php';
