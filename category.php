<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Clutch_2.0
 */

// TODO: implement page here

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

        <?php require( get_template_directory() . '/template-parts/content-category.php' ); ?> 

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
