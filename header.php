<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Clutch_2.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php if ( is_single() ) : ?>
            <!--facebook opengraph tags-->
            <?php $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST']; ?>
            <?php $image = ! empty( $image_array = array_values( get_attached_media( 'image', get_the_ID() ) ) ) ? $image_array[0]->guid : null;  ?>
            <meta property="og:title" content="<?php echo get_the_title(); ?>" />
            <?php echo '<meta property="og:image" content="http://clutch-mag.co' .  $image . '" />'; ?>
        <?php endif; ?>


        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

        <!-- todo: download this and enqueue.  better yet, get as custom font-set and enqueue -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

        <!--typekit-->
        <script src="https://use.typekit.net/iud3bia.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>

        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <style>html { margin-top: 0 !important; }</style><!--remove this for production-->

        <div id="shadow"></div>

        <?php require_once( locate_template( 'template-parts/nav/mobile-nav.php' ) ); ?>

        <div id="page" class="site">
            <?php require_once( locate_template( 'template-parts/nav/site-header.php' ) ); ?>

            <div id="content" class="site-content">
